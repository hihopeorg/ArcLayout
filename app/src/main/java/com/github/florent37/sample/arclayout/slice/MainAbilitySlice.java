package com.github.florent37.sample.arclayout.slice;

import com.github.florent37.arclayout.ArcLayout;

import com.github.florent37.sample.arclayout.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Matrix;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.util.Optional;

public class MainAbilitySlice extends AbilitySlice {
    private AnimatorValue animatorValue = new AnimatorValue();
    private Image kenBurnsView;
    private PixelMap pixelMap;

    private int restartCount = 1;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ArcLayout arcLayout = (ArcLayout) findComponentById(ResourceTable.Id_arc_layout);

        kenBurnsView = (Image) findComponentById(ResourceTable.Id_ken);
        pixelMap = decodeResource(this, ResourceTable.Media_arclayout_rogue);
    }

    @Override
    public void onActive() {
        super.onActive();
        getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                animatorValue.setDuration(10000);
                animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        if (restartCount % 2 != 0) {
                            PixelMap result = adjustRotation(pixelMap, 1 + v, 100 * v);
                            kenBurnsView.setPixelMap(result);
                        } else {
                            PixelMap result = adjustRotation(pixelMap, 2 - v, 100 - 100 * v);
                            kenBurnsView.setPixelMap(result);
                        }

                    }
                });
                animatorValue.start();
            }
        }, 2000);

        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                animatorValue.start();
                restartCount++;
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

    }

    public static PixelMap decodeResource(Context context, int id) {
        try {
            String path = context.getResourceManager().getMediaPath(id);
            if (path.isEmpty()) {
                return null;
            }
            RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            options.formatHint = "image/*";
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions)).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PixelMap adjustRotation(PixelMap map, float scale, float translate) {
        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale, (float) map.getImageInfo().size.width / 2, (float) map.getImageInfo().size.height / 2);
        matrix.postTranslate(translate, translate);
        Size size = map.getImageInfo().size;
        PixelMap pixelMap = createPixelMap(size.height, size.width);
        Paint paint = new Paint();
        Canvas canvas = new Canvas(new Texture(pixelMap));
        canvas.setMatrix(matrix);
        canvas.drawPixelMapHolder(new PixelMapHolder(map), 0, 0, paint);
        return pixelMap;

    }

    private static PixelMap createPixelMap(int width, int height) {
        PixelMap pixelMap;
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(width, height);
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        pixelMap = PixelMap.create(options);
        return pixelMap;

    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);

    }
}

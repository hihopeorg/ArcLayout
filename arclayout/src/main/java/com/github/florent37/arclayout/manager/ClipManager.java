package com.github.florent37.arclayout.manager;

import ohos.agp.render.Paint;
import ohos.agp.render.Path;

public interface ClipManager {


    Path createMask(int width, int height);

    Path getShadowConvexPath();

    void setupClipLayout(int width, int height);

    Paint getPaint();

    boolean requiresBitmap();
}

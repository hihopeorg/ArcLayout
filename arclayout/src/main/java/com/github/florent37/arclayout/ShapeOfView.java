package com.github.florent37.arclayout;

import com.github.florent37.arclayout.manager.ClipManager;
import com.github.florent37.arclayout.manager.ClipPathManager;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.Element;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;

public abstract class ShapeOfView extends StackLayout implements Component.DrawTask, ComponentContainer.ArrangeListener {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000100, "ShapeOfView");
    private final Paint clipPaint = new Paint();
    private final Path clipPath = new Path();

    private ClipManager clipManager = new ClipPathManager();
    private boolean requiersShapeUpdate = true;

    final Path rectView = new Path();

    public ShapeOfView(Context context) {
        super(context);
        init();
    }

    public ShapeOfView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public ShapeOfView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        clipPaint.setAntiAlias(true);
        clipPaint.setColor(Color.WHITE);
        clipPaint.setStyle(Paint.Style.FILL_STYLE);
        clipPaint.setStrokeWidth(1);
        setArrangeListener(this::onArrange);
        addDrawTask(this::onDraw);

    }

    @Override
    public void invalidate() {
        addDrawTask(this::onDraw);
        super.invalidate();
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        requiresShapeUpdate();
        return false;
    }


    private boolean requiresBitmap() {
        return (clipManager != null && clipManager.requiresBitmap());
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (requiersShapeUpdate) {
            calculateLayout(component.getWidth(), component.getHeight(),canvas);
            requiersShapeUpdate = false;
        }

        canvas.drawPath(rectView, clipPaint);
        HiLog.info(label, "clipPaint：%{public}d", clipPaint.getColor().getValue());
    }

    private void calculateLayout(int width, int height,Canvas canvas1) {
        rectView.reset();
        rectView.addRect(0f, 0f, 1f * getWidth(), 1f * getHeight(), Path.Direction.CLOCK_WISE);

        if (clipManager != null) {
            if (width > 0 && height > 0) {
                clipManager.setupClipLayout(width, height);
                clipPath.reset();
                clipPath.set(clipManager.createMask(width, height));
                canvas1.clipPath(clipPath, Canvas.ClipOp.DIFFERENCE);
            }
        }

        invalidate();
    }


    public void setClipPathCreator(ClipPathManager.ClipPathCreator createClipPath) {
        ((ClipPathManager) clipManager).setClipPathCreator(createClipPath);
        requiresShapeUpdate();
    }

    public void requiresShapeUpdate() {
        this.requiersShapeUpdate = true;
        invalidate();
    }

}

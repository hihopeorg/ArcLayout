package com.github.florent37.arclayout.manager;


import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;

public class ClipPathManager implements ClipManager {

    protected final Path path = new Path();
    private final Paint paint = new Paint();

    private ClipPathCreator createClipPath = null;

    public ClipPathManager() {
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(1);
    }

    public Paint getPaint() {
        return paint;
    }

    @Override
    public boolean requiresBitmap() {
        return createClipPath != null && createClipPath.requiresBitmap();
    }


    protected Path createClipPath(int width, int height) {
        if (createClipPath != null) {
            return createClipPath.createClipPath(width, height);
        }
        return null;
    }

    public void setClipPathCreator(ClipPathCreator createClipPath) {
        this.createClipPath = createClipPath;
    }

    @Override
    public Path createMask(int width, int height) {
        return path;
    }


    @Override
    public Path getShadowConvexPath() {
        return path;
    }

    @Override
    public void setupClipLayout(int width, int height) {
        path.reset();
        final Path clipPath = createClipPath(width, height);
        if (clipPath != null) {
            path.set(clipPath);
        }
    }

    public interface ClipPathCreator {
        Path createClipPath(int width, int height);

        boolean requiresBitmap();
    }
}

package com.github.florent37.arclayout;


import com.github.florent37.arclayout.manager.ClipPathManager;
import ohos.agp.components.AttrSet;
import ohos.agp.render.Path;
import ohos.app.Context;



public class ArcLayout extends ShapeOfView {

    public static final int POSITION_BOTTOM = 1;
    public static final int POSITION_TOP = 2;
    public static final int POSITION_LEFT = 3;
    public static final int POSITION_RIGHT = 4;

    public static final int CROP_INSIDE = 1;

    public static final int CROP_OUTSIDE = 2;

    private int arcPosition = POSITION_TOP;
    private int cropDirection = CROP_INSIDE;

    private int arcHeight = 0;

    public ArcLayout(Context context) {
        super(context);
        init(context, null);
    }

    public ArcLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ArcLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttrSet attrs) {
        if (attrs != null) {
            arcHeight = (int) AttrSetUtil.getDimension(attrs,"arc_height",arcHeight);
            arcPosition = getPosition(AttrSetUtil.getString(attrs,"arc_position","top"));
            cropDirection = getCrop(AttrSetUtil.getString(attrs,"arc_cropDirection","cropInside"));

        }
        super.setClipPathCreator(new ClipPathManager.ClipPathCreator() {
            @Override
            public Path createClipPath(int width, int height) {
                final Path path = new Path();

                final boolean isCropInside = cropDirection == CROP_INSIDE;

                switch (arcPosition) {
                    case POSITION_BOTTOM: {
                        if (isCropInside) {
                            path.moveTo(0, 0);
                            path.lineTo(0, height);
                            path.quadTo(width / 2, height - 2 * arcHeight, width, height);
                            path.lineTo(width, 0);
                            path.close();
                        } else {
                            path.moveTo(0, 0);
                            path.lineTo(0, height - arcHeight);
                            path.quadTo(width / 2, height + arcHeight, width, height - arcHeight);
                            path.lineTo(width, 0);
                            path.close();
                        }
                        break;
                    }
                    case POSITION_TOP:
                        if (isCropInside) {
                            path.moveTo(0, height);
                            path.lineTo(0, 0);
                            path.quadTo(width / 2, 2 * arcHeight, width, 0);
                            path.lineTo(width, height);
                            path.close();
                        } else {
                            path.moveTo(0, arcHeight);
                            path.quadTo(width / 2, -arcHeight, width, arcHeight);
                            path.lineTo(width, height);
                            path.lineTo(0, height);
                            path.close();
                        }
                        break;
                    case POSITION_LEFT:
                        if (isCropInside) {
                            path.moveTo(width, 0);
                            path.lineTo(0, 0);
                            path.quadTo(arcHeight * 2, height / 2, 0, height);
                            path.lineTo(width, height);
                            path.close();
                        } else {
                            path.moveTo(width, 0);
                            path.lineTo(arcHeight, 0);
                            path.quadTo(-arcHeight, height / 2, arcHeight, height);
                            path.lineTo(width, height);
                            path.close();
                        }
                        break;
                    case POSITION_RIGHT:
                        if (isCropInside) {
                            path.moveTo(0, 0);
                            path.lineTo(width, 0);
                            path.quadTo(width - arcHeight * 2, height / 2, width, height);
                            path.lineTo(0, height);
                            path.close();
                        } else {
                            path.moveTo(0, 0);
                            path.lineTo(width - arcHeight, 0);
                            path.quadTo(width + arcHeight, height / 2, width - arcHeight, height);
                            path.lineTo(0, height);
                            path.close();
                        }
                        break;

                }
                return path;
            }

            @Override
            public boolean requiresBitmap() {
                return false;
            }
        });
    }


    public int getArcPosition() {
        return arcPosition;
    }

    public void setArcPosition(ArcPosition arcPosition) {
        this.arcPosition = arcPosition.value;
        requiresShapeUpdate();
    }

    public int getCropDirection() {
        return cropDirection;
    }

    public void setCropDirection(CropDirection cropDirection) {
        this.cropDirection = cropDirection.value;
        requiresShapeUpdate();
    }

    public int getArcHeight() {
        return arcHeight;
    }

    public void setArcHeight(int arcHeight) {
        this.arcHeight = arcHeight;
        requiresShapeUpdate();
    }

    public  enum ArcPosition {
        BOTTOM(1),TOP(2),LEFT(3),RIGHT(4),;

        int value;

        ArcPosition(int position) {
            this.value = position;
        }
    }

    public  enum  CropDirection{
        CROP_INSIDE(1),CROP_OUTSIDE(2);
        int value;

        CropDirection(int position) {
            this.value = position;
        }
    }

    private int getPosition(String position){
        if ("bottom".equals(position)){
            return POSITION_BOTTOM;
        }else if ("left".equals(position)){
            return POSITION_LEFT;
        }else if ("right".equals(position)){
            return POSITION_RIGHT;
        }else {
            return POSITION_TOP;
        }
    }
    private  int getCrop(String crop){
        if ("cropOutside".equals(crop)){
            return CROP_OUTSIDE;
        }else {
            return CROP_INSIDE;
        }
    }
}

# ArcLayout

**本项目是基于开源项目ArcLayout进行ohos的移植和开发的，可以通过项目标签以及github地址（https://github.com/florent37/ArcLayout ）追踪到原项目版本**

#### 项目介绍

- 项目名称：ArcLayout
- 所属系列：ohos的第三方组件适配移植
- 功能：弧形容器
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/florent37/ArcLayout
- 原项目基线版本：无基线版本，sha1:8fc4b7a64ee717b8b73f54336a699a613b668179
- 编程语言：Java
- 外部库依赖：无

#### 效果
  <img src="screenshot/arc.png"/>  

#### 安装教程

方法一：

1. 编译har包ArcLayout.har。
2. 启动 DevEco Studio，将har包导入工程目录“app->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'ArcLayout', ext: 'har')

}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.github.florent37.ohos:arclayout:1.0.0'
}
```

#### 使用说明

1.底部凸型弧形容器
```xml
<com.github.florent37.arclayout.ArcLayout
            ohos:id="$+id:arc_layout"
            ohos:height="380vp"
            ohos:width="match_parent"
            app:arc_cropDirection="cropOutside"
            app:arc_height="50vp"
            app:arc_position="bottom">

           <!--内容区域-->
        </com.github.florent37.arclayout.ArcLayout>
```
2.底部凹型弧形容器
```xml
 <com.github.florent37.arclayout.ArcLayout
             ohos:id="$+id:arc_layout"
             ohos:height="380vp"
             ohos:width="match_parent"
             app:arc_cropDirection="cropInside"
             app:arc_height="50vp"
             app:arc_position="bottom">
 
            <!--内容区域-->
       </com.github.florent37.arclayout.ArcLayout>
```

#### 版本迭代
- 1.0.0

#### 版权和许可信息
```
Copyright 2016 florent37, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

